from . import models

from rest_framework import serializers


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Author
        fields = [
            'id',
            'name',
            'age',
        ]


class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Book
        fields = [
            'id',
            'name',
            'pages',
            'genre',
            'price',
            'publication_date',
            'author',
        ]


class LargeBookSerializer(serializers.ModelSerializer):
    author = AuthorSerializer()

    class Meta:
        model = models.Book
        fields = [
            'id',
            'name',
            'pages',
            'genre',
            'price',
            'publication_date',
            'author',
        ]




