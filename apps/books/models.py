from django.db import models
from django.utils.translation import ugettext_lazy as _


class Author(models.Model):
    name = models.CharField(max_length=30)
    age = models.IntegerField()


class Book(models.Model):
    DETECTIVE, NOVEL, FANTASY = 0, 1, 2
    BOOK_GENRE = (
        (DETECTIVE, 'Detective'),
        (NOVEL, 'Novel'),
        (FANTASY, 'Fantasy'),
    )
    name = models.CharField(max_length=30, verbose_name=_('Name'))
    pages = models.IntegerField()
    genre = models.PositiveSmallIntegerField(choices=BOOK_GENRE, default=DETECTIVE)
    price = models.DecimalField(max_digits=3, decimal_places=2)
    publication_date = models.DateTimeField(verbose_name='Date published')
    author = models.ForeignKey(Author, related_name='author_books')

    def __str__(self):
        return '"{0}" with {1} page(s)'.format(self.name, self.pages)

    def is_detective(self):
        return self.genre == self.DETECTIVE

    @property
    def genre_value(self):
        return self.get_genre_display()