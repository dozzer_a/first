from django.conf.urls import url
from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r'^books', views.BookViewSet)
router.register(r'^authors', views.AuthorViewSet)

urlpatterns = [
    url(r'^1_books/$', views.book_list),
    url(r'^1_create_book/$', views.book_register),
    url(r'^1_books/(?P<pk>[0-9]+)/$', views.book_actions),
    url(r'^2_books/$', views.BookList.as_view()),
    url(r'^2_books/(?P<pk>[0-9]+)/$', views.BookDetail.as_view()),
    url(r'^3_books/$', views.BookListMixin.as_view()),
    url(r'^3_books/(?P<pk>[0-9]+)/$', views.BookDetailMixin.as_view()),
    url(r'^4_books/$', views.BookListGeneric.as_view()),
    url(r'^4_books/(?P<pk>[0-9]+)/$', views.BookDetailGeneric.as_view()),
] + router.urls
