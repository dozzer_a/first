from rest_framework import status, viewsets
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import Http404
from rest_framework import mixins
from rest_framework import generics

from . import models
from . import serializers


@api_view(['GET'])
def book_list(request):
    serializer_class = serializers.LargeBookSerializer if request.GET.get('show') == 'authors' else serializers.BookSerializer
    books = models.Book.objects.all()
    serializer = serializer_class(books, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['POST'])
def book_register(request):
    serializer = serializers.BookSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT'])
def book_actions(request, pk):
    try:
        book = models.Book.objects.get(id=pk)
    except models.Book.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == 'GET':
        serializer = serializers.BookSerializer(book)
        return Response(serializer.data, status=status.HTTP_200_OK)
    elif request.method == 'PUT':
        serializer = serializers.BookSerializer(book, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# list of books, create book, update book, retrieve book


class BookList(APIView):
    def get(self, request):
        serializer_class = serializers.LargeBookSerializer if request.GET.get('show') == 'authors/' else serializers.BookSerializer
        books = models.Book.objects.all()
        serializer = serializer_class(books, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = serializers.BookSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BookDetail(APIView):
    def get_object(self, pk):
        try:
            return models.Book.objects.get(pk=pk)
        except models.Book.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        book = self.get_object(pk)
        serializer = serializers.BookSerializer(book)
        return Response(serializer.data)

    def put(self, request, pk):
        book = self.get_object(pk)
        serializer = serializers.BookSerializer(book, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BookListMixin(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):
    queryset = models.Book.objects.all()
    serializer_class = serializers.BookSerializer
    large_serializer_class = serializers.LargeBookSerializer

    def get_serializer_class(self):
        return self.large_serializer_class if self.request.GET.get('show') == 'authors' else self.serializer_class

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class BookDetailMixin(mixins.RetrieveModelMixin,
                      mixins.UpdateModelMixin,
                      generics.GenericAPIView):
    queryset = models.Book.objects.all()
    serializer_class = serializers.BookSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)


class BookListGeneric(generics.ListCreateAPIView):
    queryset = models.Book.objects.all()
    serializer_class = serializers.BookSerializer
    large_serializer_class = serializers.LargeBookSerializer

    def get_serializer_class(self):
        return self.large_serializer_class if self.request.GET.get('show') == 'authors' else self.serializer_class


class BookDetailGeneric(generics.RetrieveUpdateAPIView):
    queryset = models.Book.objects.all()
    serializer_class = serializers.BookSerializer


class BookViewSet(viewsets.ModelViewSet):
    queryset = models.Book.objects.all()
    serializer_class = serializers.BookSerializer
    permission_classes = (IsAuthenticated, )

    def get_serializer_class(self):
        return self.large_serializer_class if self.request.GET.get('show') == 'authors' else self.serializer_class

    def destroy(self, request, *args, **kwargs):
        """405"""
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def partial_update(self, request, *args, **kwargs):
        """405"""
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


class AuthorViewSet(viewsets.ModelViewSet):
    queryset = models.Author.objects.all()
    serializer_class = serializers.AuthorSerializer
