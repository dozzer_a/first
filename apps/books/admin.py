from django.contrib import admin

from .models import Book, Author



@admin.register(Book)
class BookDisplay(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'pages',
        'genre',
        'price',
        'publication_date',
        'author',
    )

    list_filter = (
        'genre',
        'author'
    )

    search_fields = (
        'name',
        'author'
    )


@admin.register(Author)
class AuthorDisplay(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'age',
    )

    search_fields = (
        'name',
        'age',
    )
